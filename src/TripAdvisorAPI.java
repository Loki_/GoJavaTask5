import java.util.Date;

/**
 * Created by anna on 27.11.16.
 */
public class TripAdvisorAPI implements API {
    Date date = new Date();
    Room[] rooms;
    @Override
    public Room[] findRooms(int price, int persons, String city, String hotel) {
        Room[] fRoom = new Room[5];
        if (rooms[0].getPrice() == price && rooms[0].getPersons() == persons && rooms[0].getCityName().equals(city) && rooms[0].getHotelName().equals(hotel))
            fRoom[0] = rooms[0];
        if (rooms[1].getPrice() == price && rooms[1].getPersons() == persons && rooms[1].getCityName().equals(city) && rooms[1].getHotelName().equals(hotel))
            fRoom[1] = rooms[1];
        if (rooms[2].getPrice() == price && rooms[2].getPersons() == persons && rooms[2].getCityName().equals(city) && rooms[2].getHotelName().equals(hotel))
            fRoom[2] = rooms[2];
        if (rooms[3].getPrice() == price && rooms[3].getPersons() == persons && rooms[3].getCityName().equals(city) && rooms[3].getHotelName().equals(hotel))
            fRoom[0] = rooms[3];
        if (rooms[4].getPrice() == price && rooms[4].getPersons() == persons && rooms[4].getCityName().equals(city) && rooms[4].getHotelName().equals(hotel))
            fRoom[4] = rooms[4];
        return fRoom;
    }
    @Override
    public Room[] getAll(){
        return this.rooms;
    }
    public TripAdvisorAPI(){
        rooms[0] = new Room(56789, 350, 7, date, "Night Pub", "Shire");
        rooms[1] = new Room(56790, 280, 4, date, "Black Snake", "Kotmandu");
        rooms[2] = new Room(56791, 140, 2, date, "White Moon", "New Deli");
        rooms[3] = new Room(56792, 100, 2, date, "America", "Moskov");
        rooms[4] = new Room(56793, 90, 3, date, "Blue night", "Samara");
    }

}
