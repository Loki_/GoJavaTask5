import java.util.Date;

/**
 * Created by anna on 27.11.16.
 */
public class GoogleAPI implements API {
    Room[] rooms;
    Date date = new Date();
    @Override
    public Room[] findRooms(int price, int persons, String city, String hotel) {
        Room[] fRoom = new Room[5];
        if (rooms[0].getPrice() == price && rooms[0].getPersons() == persons && rooms[0].getCityName().equals(city) && rooms[0].getHotelName().equals(hotel))
            fRoom[0] = rooms[0];
        if (rooms[1].getPrice() == price && rooms[1].getPersons() == persons && rooms[1].getCityName().equals(city) && rooms[1].getHotelName().equals(hotel))
            fRoom[1] = rooms[1];
        if (rooms[2].getPrice() == price && rooms[2].getPersons() == persons && rooms[2].getCityName().equals(city) && rooms[2].getHotelName().equals(hotel))
            fRoom[2] = rooms[2];
        if (rooms[3].getPrice() == price && rooms[3].getPersons() == persons && rooms[3].getCityName().equals(city) && rooms[3].getHotelName().equals(hotel))
            fRoom[0] = rooms[3];
        if (rooms[4].getPrice() == price && rooms[4].getPersons() == persons && rooms[4].getCityName().equals(city) && rooms[4].getHotelName().equals(hotel))
            fRoom[4] = rooms[4];
        return fRoom;
    }
    @Override
    public Room[] getAll(){
        return this.rooms;
    }
    public GoogleAPI(){
        rooms[0] = new Room(98765, 500, 1, date, "Hilton", "New York");
        rooms[1] = new Room(98766, 300, 1, date, "Aurora", "London");
        rooms[2] = new Room(98767, 400, 2, date, "Mechanika", "Paris");
        rooms[3] = new Room(98768, 70, 2, date, "Etnika", "Rome");
        rooms[4] = new Room(98769, 210, 3, date, "Zelena Mriya", "Kyiv");
    }


}
