import java.util.Date;

/**
 * Created by anna on 27.11.16.
 */
public class Room {
    private long id;
    private int price;
    private int persons;
    private Date dateAvailableFrom;
    private String hotelName;
    private String cityName;

    public Room(long id, int price, int persons, Date dateAvailableFrom, String hotelName, String cityName){
        this.id = id;
        this.price = price;
        this.persons = persons;
        this.dateAvailableFrom = dateAvailableFrom;
        this.hotelName = hotelName;
        this.cityName = cityName;
    }

    public long getId(){
        return this.id;
    }
    public void setId(long id){
        this.id = id;
    }
    public int getPrice(){
        return this.price;
    }
    public void setPrice(int price){
        this.price = price;
    }
    public int getPersons(){
        return this.persons;
    }
    public void setPersons(int persons){
        this.persons = persons;
    }
    public Date getDateAvailableFrom(){
        return this.dateAvailableFrom;
    }
    public void setDateAvailableFrom(Date dateAvailableFrom){
        this.dateAvailableFrom = dateAvailableFrom;
    }
    public String getHotelName(){
        return this.hotelName;
    }
    public void setHotelName(String hotelName){
        this.hotelName = hotelName;
    }
    public String getCityName(){
        return this.cityName;
    }
    public void setCityName(){
        this.cityName = cityName;
    }
    @Override
    public boolean equals(Object obj){
        final Room room = (Room) obj;
        if (this.price == room.price && this.persons == room.persons && this.hotelName.equals(room.cityName)){
            return true;
        } else
        return false;
    }
    @Override
    public int hashCode(){
        int result = 17;
        result = 31 * result + price;
        result = 31 * result + persons;
        result = 31 * result + hotelName.hashCode();
        return result;
    }
}
