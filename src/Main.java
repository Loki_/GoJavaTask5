import java.util.Date;

/**
 * Created by anna on 27.11.16.
 */
public class Main {
    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.requstRooms(300,3,"Rome","Grand");
        controller.requstRooms(400,2,"Paris","Mechanika");
        controller.requstRooms(100,2,"Moskov","America");
        for(Room r : controller.requstRooms(500,1,"New York","Hilton")){
            System.out.println(r.getId());
        }

        API api1 = new BookingComAPI();
        API api2 = new GoogleAPI();
        API api3 = new TripAdvisorAPI();
        controller.check(api1,api2);
        controller.check(api3,api2);
        controller.check(api1,api3);

        Room r1 = new Room(1, 222, 2, new Date(), "hotel", "city");
        DAO dao = new DAOImpl();
        dao.save(r1);
        dao.delete(r1);

    }
}
